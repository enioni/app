import flask
import pickle
import pandas as pd
from sklearn.externals import joblib

# Use pickle to load in the pre-trained model
with open(f'model/model.pkl', 'rb') as f:
    model =  joblib.load(f) 

#model = pd.read_pickle('APP/model/model.pkl')

# Initialise the Flask app
app = flask.Flask(__name__, template_folder='templates')

# Set up the main route
@app.route('/', methods=['GET', 'POST'])
def main():
    if flask.request.method == 'GET':
        # Just render the initial form, to get input
        return(flask.render_template('main.html'))
    
    if flask.request.method == 'POST':
        # Extract the input
        DistanceGgl = flask.request.form['DistanceGgl']
        DurationGgl = flask.request.form['DurationGgl']
        TrafficDurationGgl = flask.request.form['TrafficDurationGgl']
        TrafficDurationOptGgl = flask.request.form['TrafficDurationOptGgl']
        TruckDistance_here = flask.request.form['TruckDistance_here']
        TruckTrafficTime_here = flask.request.form['TruckTrafficTime_here']
        gh_travel_time = flask.request.form['gh_travel_time']
        gh_distance = flask.request.form['gh_distance']
        gh_steps = flask.request.form['gh_steps']
        gh_left_turns = flask.request.form['gh_left_turns']
        ascend_ratio = flask.request.form['ascend_ratio']
        primeracarga = flask.request.form['primeracarga']
        GRqty = flask.request.form['GRqty']
        elementopopular = flask.request.form['elementopopular']
        especialidadpopular = flask.request.form['especialidadpopular']
        subcanalpopular = flask.request.form['subcanalpopular']
        ZZLOAD_FREQ = flask.request.form['ZZLOAD_FREQ']
        voltotal = flask.request.form['voltotal']
        TemperatureAvgC = flask.request.form['TemperatureAvgC']
        Precipitationmm = flask.request.form['Precipitationmm']
        WindSpeedAvgKMH = flask.request.form['WindSpeedAvgKMH']
        WindSpeedMaxKMH = flask.request.form['WindSpeedMaxKMH']
        PressureMaxhPa = flask.request.form['PressureMaxhPa']
        PressureMinhPa = flask.request.form['PressureMinhPa']
        ZZUNLOAD_METH = flask.request.form['ZZUNLOAD_METH']
        diasem = flask.request.form['diasem']
        epoca = flask.request.form['epoca']
        Plnt = flask.request.form['Plnt']
        H3_Distance = flask.request.form['H3_Distance']
        H3_Time = flask.request.form['H3_Time']
        
        # Make DataFrame for model
        input_variables = pd.DataFrame([[DistanceGgl,DurationGgl,TrafficDurationGgl,TrafficDurationOptGgl,
        TruckDistance_here,TruckTrafficTime_here,
        gh_travel_time,gh_distance,gh_steps,gh_left_turns,ascend_ratio,primeracarga,GRqty,elementopopular,
        especialidadpopular,subcanalpopular,ZZLOAD_FREQ,voltotal,
        TemperatureAvgC,Precipitationmm,WindSpeedAvgKMH,WindSpeedMaxKMH,PressureMaxhPa,PressureMinhPa,ZZUNLOAD_METH,
        diasem,epoca,Plnt,H3_Distance,H3_Time]],
        columns=['DistanceGgl','DurationGgl','TrafficDurationGgl',
        'TrafficDurationOptGgl','TruckDistance_here','TruckTrafficTime_here',
        'gh_travel_time','gh_distance','gh_steps','gh_left_turns',
        'ascend_ratio','primeracarga','GRqty','elementopopular',
        'especialidadpopular','subcanalpopular','ZZLOAD_FREQ','voltotal',
        'TemperatureAvgC','Precipitationmm','WindSpeedAvgKMH',
        'WindSpeedMaxKMH','PressureMaxhPa','PressureMinhPa','ZZUNLOAD_METH',
        'diasem','epoca','Plnt','H3_Distance','H3_Time'],
        index=['input'])

        # Get the model's prediction
        
        prediction = model.predict(input_variables)[0]
    
        # Render the form again, but add in the prediction and remind user
        # of the values they input before


        return flask.render_template('main.html', original_input={'DistanceGgl':DistanceGgl,'DurationGgl':DurationGgl,'TrafficDurationGgl':TrafficDurationGgl,
        'TrafficDurationOptGgl':TrafficDurationOptGgl,'TruckDistance_here':TruckDistance_here,'TruckTrafficTime_here':TruckTrafficTime_here,
        'gh_travel_time':gh_travel_time,'gh_distance':gh_distance,'gh_steps':gh_steps,'gh_left_turns':gh_left_turns,
        'ascend_ratio':ascend_ratio,'primeracarga':primeracarga,'GRqty':GRqty,'elementopopular':elementopopular,
        'especialidadpopular':especialidadpopular,'subcanalpopular':subcanalpopular,'ZZLOAD_FREQ':ZZLOAD_FREQ,'voltotal':voltotal,
        'TemperatureAvgC':TemperatureAvgC,'Precipitationmm':Precipitationmm,'WindSpeedAvgKMH':WindSpeedAvgKMH,
        'WindSpeedMaxKMH':WindSpeedMaxKMH,'PressureMaxhPa':PressureMaxhPa,'PressureMinhPa':PressureMinhPa,'ZZUNLOAD_METH':ZZUNLOAD_METH,
        'diasem':diasem,'epoca':epoca,'Plnt':Plnt,'H3_Distance':H3_Distance,'H3_Time':H3_Time},
         result=prediction,)

if __name__ == '__main__':
    app.run()